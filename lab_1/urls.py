from django.conf.urls import url
from django.views.generic.base import RedirectView
from .views import index
#url for app
urlpatterns = [
    url(r'^$', index, name='index'),
]
