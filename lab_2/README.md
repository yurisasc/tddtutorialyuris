# What should you learn from codes & documentation in this sub-directory?

* The implementation of View: the presentation layer to the user, what you see on the browser
* The implementation of Controller: intermediary tier between model and view
* How Django Webserver Works ![How Django Webserver Works](docs/how-django-webserver-works.png)
* How Django Framework Works with Templating ![How Django Framework Works with Templating](docs/how-django-framework-works.png)
* How Django Framework Works with Templating & Models ![How Django Framework Works with Templating & Models](docs/how-django-framework-with-models-works.png)

# References

* Class Material "02. Intro to Framework, Server Side Programming with Django", Slide 1-31
* https://www.djangoproject.com/
* https://docs.djangoproject.com/en/1.11/
* https://docs.djangoproject.com/en/1.11/topics/http/urls/
